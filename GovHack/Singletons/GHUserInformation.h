//
//  GHUserInformation.h
//  GovHack
//
//  Created by Luke Scholefield on 12/07/2014.
//  Copyright (c) 2014 Luke Scholefield. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GHUserInformation : NSObject

+ (instancetype)sharedInstance;

@end
