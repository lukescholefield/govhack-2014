//
//  GHAppearanceManager.h
//  GovHack
//
//  Created by Luke Scholefield on 12/07/2014.
//  Copyright (c) 2014 Luke Scholefield. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GHAppearanceManager : NSObject

+ (instancetype)sharedInstance;

@property (assign, nonatomic, readonly) CGFloat padding;

@end
