//
//  GHAppearanceManager.m
//  GovHack
//
//  Created by Luke Scholefield on 12/07/2014.
//  Copyright (c) 2014 Luke Scholefield. All rights reserved.
//

#import "GHAppearanceManager.h"

@implementation GHAppearanceManager

+ (instancetype)sharedInstance
{
    static GHAppearanceManager *sharedInstance;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[GHAppearanceManager alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _padding = 10.0;
    }
    return self;
}
 

@end
