//
//  GHUserInformation.m
//  GovHack
//
//  Created by Luke Scholefield on 12/07/2014.
//  Copyright (c) 2014 Luke Scholefield. All rights reserved.
//

#import "GHUserInformation.h"

@implementation GHUserInformation

+ (instancetype)sharedInstance
{
    static GHUserInformation *sharedInstance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[GHUserInformation alloc] init];
    });
    return sharedInstance;
}

/*
 
 - (void)setLocations:(NSArray *)locations
 {
 locations = locations;
 
 NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
 
 if (standardUserDefaults) {
 [standardUserDefaults setObject:locations forKey:@"Locations"];
 [standardUserDefaults synchronize];
 }
 }
 */

@end
