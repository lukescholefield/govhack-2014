//
//  GHElectorateTableViewCell.h
//  GovHack
//
//  Created by Luke Scholefield on 12/07/2014.
//  Copyright (c) 2014 Luke Scholefield. All rights reserved.
//

#import "GHTableViewCell.h"
#import <MapKit/MapKit.h>
@interface GHElectorateTableViewCell : GHTableViewCell <MKMapViewDelegate>

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *subtitleLabel;
@property (strong, nonatomic) UILabel *heldByLabel;
@property (strong, nonatomic) MKMapView *mapView;

@end
