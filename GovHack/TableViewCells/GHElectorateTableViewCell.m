//
//  GHElectorateTableViewCell.m
//  GovHack
//
//  Created by Luke Scholefield on 12/07/2014.
//  Copyright (c) 2014 Luke Scholefield. All rights reserved.
//

#import "GHElectorateTableViewCell.h"

@implementation GHElectorateTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(10.0, 60.0, self.width - 2 * 10.0, 200.0)];
        self.mapView.zoomEnabled = NO;
        self.mapView.scrollEnabled = NO;
        self.mapView.userInteractionEnabled = NO;
        [self addSubview:self.mapView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 10.0, self.width - 20.0, 20.0)];
        self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0];
        [self addSubview:self.titleLabel];
        self.titleLabel.textColor = [UIColor grayColor];
        self.subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 30.0, self.width - 20.0, 20.0)];
        self.subtitleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
        [self addSubview:self.subtitleLabel];
        self.subtitleLabel.textColor = [UIColor blackColor];


        self.heldByLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 30.0, self.width - 20.0, 20.0)];
        self.heldByLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
        [self addSubview:self.heldByLabel];
        self.heldByLabel.textAlignment = NSTextAlignmentRight;
        self.heldByLabel.textColor = [UIColor blackColor];
    }
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    NSArray *pointsArray = [self.mapView overlays];
    
    [self.mapView removeOverlays:pointsArray];

}


+ (CGFloat)heightForRow
{
    return 270.0;
}

@end
