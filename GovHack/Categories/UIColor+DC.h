//
//  UIColor+DC.h
//  Timely
//
//  Created by Luke Scholefield on 5/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (DC)

+ (UIColor *)colorWithHex:(NSInteger)hexValue;
+ (UIColor *)colorWithHex:(NSInteger)hexValue alpha:(CGFloat)alpa;

@end
