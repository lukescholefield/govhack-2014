//
//  UIView+DC.h
//  Timely
//
//  Created by Luke Scholefield on 3/10/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (DC)

@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;


@property (nonatomic, assign) CGFloat left;
@property (nonatomic, assign) CGFloat right;
@property (nonatomic, assign) CGFloat top;
@property (nonatomic, assign) CGFloat bottom;


@property (nonatomic, assign) CGPoint origin;
@property (nonatomic, assign) CGSize size;
@end