//
//  MKPolygon+DC.h
//  GovHack
//
//  Created by Luke Scholefield on 12/07/2014.
//  Copyright (c) 2014 Luke Scholefield. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKPolygon (DC)

- (BOOL)containsCoordinate:(CLLocationCoordinate2D)coord;
- (BOOL)containsMapPoint:(MKMapPoint)mapPoint;

@end
