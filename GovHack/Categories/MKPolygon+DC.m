//
//  MKPolygon+DC.m
//  GovHack
//
//  Created by Luke Scholefield on 12/07/2014.
//  Copyright (c) 2014 Luke Scholefield. All rights reserved.
//

// credit to http://stackoverflow.com/questions/4354130/how-to-determine-if-an-annotation-is-inside-of-mkpolygonview-ios

#import "MKPolygon+DC.h"

@implementation MKPolygon (DC)

-(BOOL)containsCoordinate:(CLLocationCoordinate2D)coord
{
    MKMapPoint mapPoint = MKMapPointForCoordinate(coord);
    return [self containsMapPoint:mapPoint];
}

- (BOOL)containsMapPoint:(MKMapPoint)mapPoint
{
    MKPolygonRenderer *polygonRenderer = [[MKPolygonRenderer alloc] initWithPolygon:self];
    CGPoint polygonViewPoint = [polygonRenderer pointForMapPoint:mapPoint];
    return CGPathContainsPoint(polygonRenderer.path, NULL, polygonViewPoint, NO);
}


@end
