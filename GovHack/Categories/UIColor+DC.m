//
//  UIColor+DC.m
//  Timely
//
//  Created by Luke Scholefield on 5/06/13.
//  Copyright (c) 2013 Luke Scholefield. All rights reserved.
//

#import "UIColor+DC.h"

@implementation UIColor (DC)

+ (UIColor *)colorWithHex:(NSInteger)hexValue
{
    return [UIColor colorWithHex:hexValue alpha:1.0f];
}

+ (UIColor *)colorWithHex:(NSInteger)hexValue alpha:(CGFloat)alpha
{
    // really need to confirm that this is actually doing integer division... should I change this to >> bit shift?
    return [UIColor colorWithRed:(hexValue/(256*256))/255.0 green:((hexValue/256)%256)/255.0 blue:(hexValue%256)/255.0 alpha:alpha];
}

@end
