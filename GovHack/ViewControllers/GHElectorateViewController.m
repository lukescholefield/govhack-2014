//
//  GHElectorateViewController.m
//  GovHack
//
//  Created by Luke Scholefield on 12/07/2014.
//  Copyright (c) 2014 Luke Scholefield. All rights reserved.
//

#import "GHElectorateViewController.h"
#import "GHPollingPlaceTableViewCell.h"
#import "GHPollingPlaceViewController.h"

@interface GHElectorateViewController ()

@property (assign, nonatomic) CFTimeInterval animationTime;

@end

@implementation GHElectorateViewController


- (void)initData
{
    self.data = @[@{@"Candidate" : @"Kate ELLIS", @"Party" : @"ALP", @"2PP" : @(53.95)},
                  @{@"Candidate" : @"Carmen GARCIA", @"Party" : @"Liberal", @"2PP" : @(46.05)}];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Adelaide";
    [self setupData];
    self.animationTime = 4.0;
    
    [self initData];
    CGFloat headerHeight = 200.0;
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.tableView registerClass:[GHPollingPlaceTableViewCell class] forCellReuseIdentifier:[GHPollingPlaceTableViewCell reuseIdentifier]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    CGFloat votesLabelWidth = 120.0;

    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.width, votesLabelWidth+ 30+10+ 5)];
    
    self.firstCandidateVotesLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 10.0, votesLabelWidth, votesLabelWidth)];
    self.firstCandidateVotesLabel.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:48.0];
    self.firstCandidateVotesLabel.text = @"0.0%";
    self.firstCandidateVotesLabel.textAlignment = NSTextAlignmentCenter;
    [self.tableView.tableHeaderView addSubview:self.firstCandidateVotesLabel];

    
    self.firstCandidateNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.firstCandidateVotesLabel.left, self.firstCandidateVotesLabel.bottom, self.firstCandidateVotesLabel.width, 30.0)];
    self.firstCandidateNameLabel.text = self.data[0][@"Candidate"];
    self.firstCandidateNameLabel.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:16.0];
    self.firstCandidateNameLabel.textAlignment = NSTextAlignmentCenter;

    [self.tableView.tableHeaderView addSubview:self.firstCandidateNameLabel];
    

    self.secondCandidateVotesLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.width - 10.0 - votesLabelWidth, 10.0, votesLabelWidth, votesLabelWidth)];
    self.secondCandidateVotesLabel.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:48.0];
    self.secondCandidateVotesLabel.text = @"0.0%";
    self.secondCandidateVotesLabel.textAlignment = NSTextAlignmentCenter;
    [self.tableView.tableHeaderView addSubview:self.secondCandidateVotesLabel];

    self.secondCandidateNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.secondCandidateVotesLabel.left, self.secondCandidateVotesLabel.bottom, self.secondCandidateVotesLabel.width, 30.0)];
    self.secondCandidateNameLabel.text = self.data[1][@"Candidate"];
    self.secondCandidateNameLabel.font = [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:16.0];
    self.secondCandidateNameLabel.textAlignment = NSTextAlignmentCenter;
    [self.tableView.tableHeaderView addSubview:self.secondCandidateNameLabel];
    
    
    CGFloat graphLineWidth = 1.0;
    self.firstCandidateTotalLayer = [[CAShapeLayer alloc] init];
    self.firstCandidateTotalLayer.frame = self.firstCandidateVotesLabel.bounds;
    self.firstCandidateTotalLayer.strokeColor = [[UIColor colorWithHex:0xCB0F32] CGColor];
    self.firstCandidateTotalLayer.fillColor = [[UIColor clearColor] CGColor];
    self.firstCandidateTotalLayer.lineWidth = graphLineWidth;
    CGMutablePathRef waitingPath = CGPathCreateMutable();
    CGPathAddArc(waitingPath, NULL, votesLabelWidth/2.0, votesLabelWidth/2.0, votesLabelWidth/2.0 - graphLineWidth/2.0, -M_PI/2, -M_PI/2+ 2*M_PI, true);
    self.firstCandidateTotalLayer.path = waitingPath;
    self.firstCandidateTotalLayer.strokeStart = 0.0;
    self.firstCandidateTotalLayer.strokeEnd = 0.0;
    [self.firstCandidateVotesLabel.layer addSublayer:self.firstCandidateTotalLayer];

    
    self.secondCandidateTotalLayer = [[CAShapeLayer alloc] init];
    self.secondCandidateTotalLayer.frame = self.secondCandidateVotesLabel.bounds;
    self.secondCandidateTotalLayer.strokeColor = [[UIColor colorWithHex:0x006FB9] CGColor];
    self.secondCandidateTotalLayer.fillColor = [[UIColor clearColor] CGColor];
    self.secondCandidateTotalLayer.lineWidth = graphLineWidth;
    waitingPath = CGPathCreateMutable();
    CGPathAddArc(waitingPath, NULL, votesLabelWidth/2.0, votesLabelWidth/2.0, votesLabelWidth/2.0 - graphLineWidth/2.0, -M_PI/2, -M_PI/2+ 2*M_PI, false);
    self.secondCandidateTotalLayer.path = waitingPath;
    self.secondCandidateTotalLayer.strokeStart = 0.0;
    self.secondCandidateTotalLayer.strokeEnd = 0.0;
    [self.secondCandidateVotesLabel.layer addSublayer:self.secondCandidateTotalLayer];


}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(updateLabels:)];
    [self.displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

- (void)updateLabels:(CADisplayLink *)displayLink
{

    if (self.initialTimeInterval == 0) {
        self.initialTimeInterval = self.displayLink.timestamp;

        CGFloat candidateShare = [self.data[0][@"2PP"] doubleValue]/100.0;

        
        [CATransaction begin];
        [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        [CATransaction setAnimationDuration:self.animationTime * candidateShare];
        self.firstCandidateTotalLayer.strokeEnd = candidateShare;
        [CATransaction commit];
        
        candidateShare = [self.data[1][@"2PP"] doubleValue]/100.0;
        
        [CATransaction begin];
        [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
        [CATransaction setAnimationDuration:self.animationTime * candidateShare];
        self.secondCandidateTotalLayer.strokeEnd = candidateShare;
        [CATransaction commit];
        
    } else if (self.displayLink.timestamp - self.initialTimeInterval <= self.animationTime) {
        CFTimeInterval elapsedTime = self.displayLink.timestamp - self.initialTimeInterval;
        
        CGFloat completion = elapsedTime/self.animationTime * 100.0;
        
        CGFloat candidateVotes = [self.data[0][@"2PP"] doubleValue];
        if (completion < candidateVotes) {
            self.firstCandidateVotesLabel.text = [NSString stringWithFormat:@"%0.1f", completion];
        } else {
            self.firstCandidateVotesLabel.text = [NSString stringWithFormat:@"%0.1f", candidateVotes];
        }

        candidateVotes = [self.data[1][@"2PP"] doubleValue];
        if (completion < candidateVotes) {
            self.secondCandidateVotesLabel.text = [NSString stringWithFormat:@"%0.1f", completion];
        } else {
            self.secondCandidateVotesLabel.text = [NSString stringWithFormat:@"%0.1f", candidateVotes];
        }

        
    } else {
        [self.displayLink removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        self.displayLink = nil;
    }
}

- (void)setupData
{
    self.pollingBooths = @[@{@"name" : @"Adelaide (Adelaide)", @"lat" : @(-34.9226396), @"lng" : @(138.5993915)},
                           @{@"name" : @"Adelaide ADELAIDE PPVC", @"lat" : @(-34.9226396), @"lng" : @(138.5993915)},
                                                      @{@"name" : @"Adelaide East", @"lat" : @(-34.92788), @"lng" : @(138.60935)},
                                                      @{@"name" : @"Adelaide Hospital", @"lat" : @(-34.9204), @"lng" : @(138.609)},
                                                      @{@"name" : @"Adelaide South", @"lat" : @(-34.934185), @"lng" : @(138.604438)},
                                                      @{@"name" : @"Adelaide West", @"lat" : @(-34.9281), @"lng" : @(138.589)},
                                                      @{@"name" : @"Black Forest East", @"lat" : @(-34.9605504), @"lng" : @(138.5810571)},
                                                      @{@"name" : @"Black Forest West", @"lat" : @(-34.960414), @"lng" : @(138.573518)},
                                                      @{@"name" : @"Blair Athol", @"lat" : @(-34.8616), @"lng" : @(138.599)},
                                                      @{@"name" : @"Blair Athol North", @"lat" : @(-34.8542), @"lng" : @(138.599)},
                                                      @{@"name" : @"BLV Adelaide PPVC", @"lat" : @(-34.921832), @"lng" : @(138.598961)},
                                                      @{@"name" : @"Broadview", @"lat" : @(-34.880086), @"lng" : @(138.614978)},
                                                      @{@"name" : @"Brompton", @"lat" : @(-34.8979), @"lng" : @(138.576)},
                                                      @{@"name" : @"Clarence Park", @"lat" : @(-34.9643), @"lng" : @(138.59)},
                                                      @{@"name" : @"College Park", @"lat" : @(-34.9076), @"lng" : @(138.617)},
                                                      @{@"name" : @"Croydon (Adelaide)", @"lat" : @(-34.8944), @"lng" : @(138.568)},
                                                      @{@"name" : @"Croydon Park", @"lat" : @(-34.88015), @"lng" : @(138.56602)},
                                                      @{@"name" : @"Divisional Office (PREPOLL)", @"lat" : @(-34.9217572), @"lng" : @(138.5992614)},
                                                      @{@"name" : @"Dulwich", @"lat" : @(-34.9345), @"lng" : @(138.632)},
                                                      @{@"name" : @"Enfield", @"lat" : @(-34.8655), @"lng" : @(138.602)},
                                                      @{@"name" : @"Enfield North", @"lat" : @(-34.851301), @"lng" : @(138.603859)},
                                                      @{@"name" : @"Fullarton", @"lat" : @(-34.95773), @"lng" : @(138.62051)},
                                                      @{@"name" : @"Glynde ADELAIDE PPVC", @"lat" : @(-34.899114), @"lng" : @(138.657211)},
                                                      @{@"name" : @"Goodwood", @"lat" : @(-34.9515), @"lng" : @(138.589)},
                                                      @{@"name" : @"Goodwood Park", @"lat" : @(-34.95437), @"lng" : @(138.59077)},
                                                      @{@"name" : @"Greenacres", @"lat" : @(-34.871), @"lng" : @(138.626)},
                                                      @{@"name" : @"Hampstead Gardens", @"lat" : @(-34.871555), @"lng" : @(138.622197)},
                                                      @{@"name" : @"Hindmarsh", @"lat" : @(-34.90425), @"lng" : @(138.56967)},
                                                      @{@"name" : @"Hyde Park", @"lat" : @(-34.9576), @"lng" : @(138.607)},
                                                      @{@"name" : @"Kent Town", @"lat" : @(-34.91925), @"lng" : @(138.62267)},
                                                      @{@"name" : @"Keswick", @"lat" : @(-34.94303), @"lng" : @(138.57221)},
                                                      @{@"name" : @"Kilburn", @"lat" : @(-34.86216), @"lng" : @(138.588163)},
                                                      @{@"name" : @"Kilkenny North (Adelaide)", @"lat" : @(-34.87382), @"lng" : @(138.555193)},
                                                      @{@"name" : @"Lower North Adelaide", @"lat" : @(-34.90603), @"lng" : @(138.608531)},
                                                      @{@"name" : @"Malvern", @"lat" : @(-34.9584), @"lng" : @(138.612)},
                                                      @{@"name" : @"Marden (Adelaide)", @"lat" : @(-34.89664), @"lng" : @(138.63846)},
                                                      @{@"name" : @"Maylands", @"lat" : @(-34.908307), @"lng" : @(138.639604)},
                                                      @{@"name" : @"Mile End (Adelaide)", @"lat" : @(-34.91979), @"lng" : @(138.53797)},
                                                      @{@"name" : @"Nailsworth", @"lat" : @(-34.882716), @"lng" : @(138.603923)},
                                                      @{@"name" : @"North Adelaide", @"lat" : @(-34.905106), @"lng" : @(138.597604)},
                                                      @{@"name" : @"Northfield", @"lat" : @(-34.848419), @"lng" : @(138.619864)},
                                                      @{@"name" : @"Northgate (Adelaide)", @"lat" : @(-34.8583415), @"lng" : @(138.6266987)},
                                                      @{@"name" : @"Norwood (Adelaide)", @"lat" : @(-34.9216), @"lng" : @(138.636)},
                                                      @{@"name" : @"Norwood West", @"lat" : @(-34.9188), @"lng" : @(138.631)},
                                                      @{@"name" : @"Parkside", @"lat" : @(-34.9446), @"lng" : @(138.62)},
                                                      @{@"name" : @"Prospect", @"lat" : @(-34.884989), @"lng" : @(138.590019)},
                                                      @{@"name" : @"Prospect North", @"lat" : @(-34.87485), @"lng" : @(138.596842)},
                                                      @{@"name" : @"Prospect South", @"lat" : @(-34.8901), @"lng" : @(138.602)},
                                                      @{@"name" : @"Renown Park", @"lat" : @(-34.8944), @"lng" : @(138.581)},
                                                      @{@"name" : @"Rose Park", @"lat" : @(-34.93144), @"lng" : @(138.62877)},
                                                      @{@"name" : @"St Peters", @"lat" : @(-34.90694), @"lng" : @(138.62487)},
                                                      @{@"name" : @"St Peters East", @"lat" : @(-34.905367), @"lng" : @(138.6277398)},
                                                      @{@"name" : @"Special Hospital Team", @"lat" : @(0), @"lng" : @(0)},
                                                      @{@"name" : @"Unley", @"lat" : @(-34.9508), @"lng" : @(138.608)},
                                                      @{@"name" : @"Vale Park (Adelaide)", @"lat" : @(-34.88542), @"lng" : @(138.62594)},
                                                      @{@"name" : @"Walkerville", @"lat" : @(-34.8953), @"lng" : @(138.613)},
                                                      @{@"name" : @"Walkerville North", @"lat" : @(-34.8876), @"lng" : @(138.616)},
                                                      @{@"name" : @"Wayville", @"lat" : @(-34.94251), @"lng" : @(138.59601)},
                           ];
}
                        
                        
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.pollingBooths count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GHPollingPlaceTableViewCell *cell = (GHPollingPlaceTableViewCell *)[tableView dequeueReusableCellWithIdentifier:[GHPollingPlaceTableViewCell reuseIdentifier]
                                                                                                       forIndexPath:indexPath];    
    cell.textLabel.text = self.pollingBooths[indexPath.row][@"name"];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [GHPollingPlaceTableViewCell heightForRow];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    GHPollingPlaceViewController *pollingPlaceViewController = [[GHPollingPlaceViewController alloc] init];
    pollingPlaceViewController.dictionary = self.pollingBooths[indexPath.row];
    [self.navigationController pushViewController:pollingPlaceViewController animated:YES];
}


@end
