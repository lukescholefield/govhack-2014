//
//  GHPollingPlaceViewController.m
//  GovHack
//
//  Created by Luke Scholefield on 12/07/2014.
//  Copyright (c) 2014 Luke Scholefield. All rights reserved.
//

#import "GHPollingPlaceViewController.h"

@interface GHPollingPlaceViewController ()

@end

@implementation GHPollingPlaceViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.title = self.dictionary[@"name"];
    
    self.mapView  = [[MKMapView alloc] initWithFrame:self.view.bounds];
    self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.mapView.delegate = self;
    [self.view addSubview:self.mapView];

    
    //
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    self.coordinate = CLLocationCoordinate2DMake([self.dictionary[@"lat"] floatValue], [self.dictionary[@"lng"] floatValue]);

    annotation.coordinate = self.coordinate;
    [self.mapView addAnnotation:annotation];

    self.mapView.centerCoordinate = annotation.coordinate;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(self.coordinate, 800, 800)];
    adjustedRegion.span.longitudeDelta  = 0.005;
    adjustedRegion.span.latitudeDelta  = 0.005;
    [self.mapView setRegion:adjustedRegion animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    static NSString* AnnotationIdentifier = @"AnnotationIdentifier";
    MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationIdentifier];
    if(annotationView) {
        return annotationView;
        
    } else {
        MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                                        reuseIdentifier:AnnotationIdentifier];
        annotationView.canShowCallout = YES;
        annotationView.image = [UIImage imageNamed:@"pin"];
        annotationView.draggable = YES;
        annotationView.centerOffset =  CGPointMake(0, -annotationView.image.size.height/2.0);
        return annotationView;
    }
    return nil;
}



@end
