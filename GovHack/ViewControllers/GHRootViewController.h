//
//  GHRootViewController.h
//  GovHack
//
//  Created by Luke Scholefield on 12/07/2014.
//  Copyright (c) 2014 Luke Scholefield. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "SimpleKML.h"

@interface GHRootViewController : UIViewController <MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (assign, nonatomic) CLLocationCoordinate2D targetCoordinate;

@property (strong, nonatomic) MKMapView *mapView;
@property (strong, nonatomic) UIView *containerView;

@property (strong, nonatomic) UIScrollView *modeScrollView;
@property (strong, nonatomic) UITableView *politicalTableView;

@property (strong, nonatomic) UIButton *politicalButton;
@property (strong, nonatomic) UIButton *peopleButton;
@property (strong, nonatomic) UIButton *historyButton;
@property (strong, nonatomic) UIButton *environmentButton;

@property (strong, nonatomic) UITapGestureRecognizer *mapViewGestureRecognizer;

@property (strong, nonatomic) SimpleKML *localGovernment;
@property (strong, nonatomic) SimpleKML *federalElectorates;
@property (strong, nonatomic) SimpleKML *stateElectorates;

@end
