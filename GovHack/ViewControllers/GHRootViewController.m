//
//  GHRootViewController.m
//  GovHack
//
//  Created by Luke Scholefield on 12/07/2014.
//  Copyright (c) 2014 Luke Scholefield. All rights reserved.
//

#import "GHRootViewController.h"
#import "SimpleKML.h"
#import "SimpleKMLContainer.h"
#import "SimpleKMLDocument.h"
#import "SimpleKMLFeature.h"
#import "SimpleKMLPlacemark.h"
#import "SimpleKMLPoint.h"
#import "SimpleKMLPolygon.h"
#import "SimpleKMLLinearRing.h"
#import "MKPolygon+DC.h"
#import "GHElectorateTableViewCell.h"
#import "GHElectorateViewController.h"

@interface GHRootViewController ()

@property (assign, nonatomic) BOOL mapExpanded;

@end

@implementation GHRootViewController

- (void)viewDidLoad
{
    self.targetCoordinate = CLLocationCoordinate2DMake(-34.9290, 138.6010);

    self.mapExpanded = YES;
    self.title = @"Civicality";
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
    self.mapView.delegate = self;
    [self.view addSubview:self.mapView];
    
    
    self.mapViewGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mapViewTapped:)];
    [self.mapView addGestureRecognizer:self.mapViewGestureRecognizer];
    
    self.containerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, self.view.height, self.view.width, self.view.height)];
    [self.view addSubview:self.containerView];
    
    self.modeScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.width, 44.0)];
    [self.containerView addSubview:self.modeScrollView];

    
    self.politicalButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 80.0, self.modeScrollView.height)];
    [self.politicalButton setTitleColor:[UIColor grayColor]  forState:UIControlStateNormal];
    [self.politicalButton setTitle:NSLocalizedString(@"Political", nil) forState:UIControlStateNormal];
    [self.politicalButton addTarget:self action:@selector(politicalButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    self.politicalButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
    [self.modeScrollView addSubview:self.politicalButton];

    self.peopleButton = [[UIButton alloc] initWithFrame:CGRectMake(self.politicalButton.right, 0.0, 80.0, self.modeScrollView.height)];
    [self.peopleButton setTitleColor:[UIColor colorWithHex:0x6AB6FE]  forState:UIControlStateNormal];
    [self.peopleButton setTitle:NSLocalizedString(@"People", nil) forState:UIControlStateNormal];
    [self.peopleButton addTarget:self action:@selector(politicalButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    self.peopleButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
    [self.modeScrollView addSubview:self.peopleButton];
    
    
    self.historyButton = [[UIButton alloc] initWithFrame:CGRectMake(self.peopleButton.right, 0.0, 80.0, self.modeScrollView.height)];
    [self.historyButton setTitleColor:[UIColor colorWithHex:0x6AB6FE]  forState:UIControlStateNormal];
    [self.historyButton setTitle:NSLocalizedString(@"History", nil) forState:UIControlStateNormal];
    [self.historyButton addTarget:self action:@selector(politicalButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    self.historyButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
    [self.modeScrollView addSubview:self.historyButton];
    
    self.environmentButton = [[UIButton alloc] initWithFrame:CGRectMake(self.historyButton.right, 0.0, 80.0, self.modeScrollView.height)];
    [self.environmentButton setTitleColor:[UIColor colorWithHex:0x6AB6FE]  forState:UIControlStateNormal];
    [self.environmentButton setTitle:NSLocalizedString(@"Environment", nil) forState:UIControlStateNormal];
    [self.environmentButton addTarget:self action:@selector(politicalButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    self.environmentButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.0];
    [self.modeScrollView addSubview:self.environmentButton];
    

    
    self.politicalTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, self.modeScrollView.bottom, self.view.width, self.containerView.height - self.modeScrollView.bottom)];
    [self.politicalTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.politicalTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.politicalTableView registerClass:[GHElectorateTableViewCell class] forCellReuseIdentifier:[GHElectorateTableViewCell reuseIdentifier]];
    self.politicalTableView.delegate = self;
    self.politicalTableView.dataSource = self;
    [self.containerView addSubview:self.politicalTableView];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    
    annotation.coordinate = self.targetCoordinate;
    
    [self.mapView addAnnotation:annotation];

    
    NSError *error = nil;
    self.federalElectorates = [SimpleKML KMLWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sa_federal_electorates" ofType:@"kml"] error:&error];
    self.localGovernment = [SimpleKML KMLWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sa_local_government" ofType:@"kml"] error:&error];
    self.stateElectorates = [SimpleKML KMLWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"sa_state_electorates" ofType:@"kml"] error:&error];
    
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(self.targetCoordinate, 800, 800)];
    adjustedRegion.span.longitudeDelta  = 0.05;
    adjustedRegion.span.latitudeDelta  = 0.05;
    [self.mapView setRegion:adjustedRegion animated:YES];
}

- (void)mapViewTapped:(id)sender
{
    if (self.mapExpanded) {
        // position marker
        self.mapExpanded = NO;
    } else {
        self.mapExpanded = YES;
    }
}

- (void)setMapExpanded:(BOOL)mapExpanded
{
    _mapExpanded = mapExpanded;
    
    if (mapExpanded) {
        self.containerView.height = self.view.height;
   
        [UIView animateWithDuration:0.6
                              delay:0.0
             usingSpringWithDamping:0.6
              initialSpringVelocity:2.0
                            options:0
                         animations:^{
                             self.mapView.height = 150.0;
                             self.containerView.top = 150.0;
                         }
                         completion:^(BOOL completed){
                             self.containerView.height = self.view.height - 150.0;
                         }];

    } else {
        [UIView animateWithDuration:0.4
                              delay:0.0
             usingSpringWithDamping:1.0
              initialSpringVelocity:2.0
                            options:0
                         animations:^{
                             self.containerView.top = self.view.height;
                             self.mapView.height = self.view.height;

                         }
                         completion:^(BOOL completed){
                         }];
    }
}

#pragma mark - Target methods


- (void)politicalButtonTapped:(id)sender
{
    #warning TODO
}

#pragma mark - MKMapViewDelegate


- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if (![overlay isKindOfClass:[MKPolygon class]]) {
        return nil;
    }
    MKPolygon *polygon = (MKPolygon *)overlay;
    MKPolygonRenderer *renderer = [[MKPolygonRenderer alloc] initWithPolygon:polygon];
    renderer.fillColor = [[UIColor colorWithHex:0x6AB6FE] colorWithAlphaComponent:0.7];
    return renderer;
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    static NSString* AnnotationIdentifier = @"AnnotationIdentifier";
    MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationIdentifier];
    if(annotationView) {
        return annotationView;

    } else {
        MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                                         reuseIdentifier:AnnotationIdentifier];
        annotationView.canShowCallout = YES;
        annotationView.image = [UIImage imageNamed:@"pin"];
        annotationView.draggable = YES;
        annotationView.centerOffset =  CGPointMake(0, -annotationView.image.size.height/2.0);
        return annotationView;
    }
    return nil;
}



#pragma mark - UITableViewDelegate/UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GHElectorateTableViewCell *cell = (GHElectorateTableViewCell *)[tableView dequeueReusableCellWithIdentifier:[GHElectorateTableViewCell reuseIdentifier]
                                                                                                   forIndexPath:indexPath];
    
    SimpleKML *kml = nil;
    
    cell.subtitleLabel.text = @"Adelaide";
    
    
    if (indexPath.row == 0) {
        kml = self.federalElectorates;
        cell.titleLabel.text = @"Federal";
        cell.heldByLabel.text = @"Kate ELLIS";
    } else if (indexPath.row == 1) {
        kml = self.stateElectorates;
        cell.titleLabel.text = @"State";
        cell.heldByLabel.text = @"Rachel SANDERSON";
    } else if (indexPath.row == 2) {
        kml = self.localGovernment;
        cell.titleLabel.text = @"Local";
        cell.subtitleLabel.text = @"Adelaide City Council";
        cell.heldByLabel.text = @"Stephen YARWOOD";
    }
    
    cell.mapView.delegate = self;
    
    if (kml.feature && [kml.feature isKindOfClass:[SimpleKMLDocument class]])
    {
        // see if the document has features of its own
        //
        for (SimpleKMLFeature *feature in ((SimpleKMLContainer *)kml.feature).features)
        {
            if ([feature isKindOfClass:[SimpleKMLPlacemark class]] && ((SimpleKMLPlacemark *)feature).point)
            {
                SimpleKMLPoint *point = ((SimpleKMLPlacemark *)feature).point;
                
                // create a normal point annotation for it
                //
                MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
                
                annotation.coordinate = point.coordinate;
                annotation.title      = feature.name;
                
                [self.mapView addAnnotation:annotation];
            }
            
            // otherwise, see if we have any placemark features with a polygon
            //
            else if ([feature isKindOfClass:[SimpleKMLPlacemark class]] && ((SimpleKMLPlacemark *)feature).polygon)
            {
                SimpleKMLPolygon *polygon = (SimpleKMLPolygon *)((SimpleKMLPlacemark *)feature).polygon;
                
                SimpleKMLLinearRing *outerRing = polygon.outerBoundary;
                
                CLLocationCoordinate2D points[[outerRing.coordinates count]];
                NSUInteger i = 0;
                
                for (CLLocation *coordinate in outerRing.coordinates)
                    points[i++] = coordinate.coordinate;
                
                // create a polygon annotation for it
                //
                MKPolygon *overlayPolygon = [MKPolygon polygonWithCoordinates:points count:[outerRing.coordinates count]];
                
                if ([overlayPolygon containsCoordinate:self.targetCoordinate]) {
                    [cell.mapView addOverlay:overlayPolygon];
                    NSLog(@"%@", feature.extendedData);
                    // zoom the map to the polygon bounds
                    //
                    [cell.mapView setVisibleMapRect:overlayPolygon.boundingMapRect animated:NO];
                }
            }
        }
    }

    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [GHElectorateTableViewCell heightForRow];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:self];
    GHElectorateViewController *electorateViewController = [[GHElectorateViewController alloc] init];
    [self.navigationController pushViewController:electorateViewController animated:YES];
}

@end
