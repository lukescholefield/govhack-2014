//
//  GHViewController.m
//  GovHack
//
//  Created by Luke Scholefield on 12/07/2014.
//  Copyright (c) 2014 Luke Scholefield. All rights reserved.
//

#import "GHViewController.h"

@interface GHViewController ()

@property (assign, nonatomic) CGFloat padding;

@end

@implementation GHViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.padding = [GHAppearanceManager sharedInstance].padding;
    }
    return self;
}


@end
