//
//  GHElectorateViewController.h
//  GovHack
//
//  Created by Luke Scholefield on 12/07/2014.
//  Copyright (c) 2014 Luke Scholefield. All rights reserved.
//

#import "GHViewController.h"

@interface GHElectorateViewController : GHViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSArray *data;

@property (assign, nonatomic) CFTimeInterval initialTimeInterval;
@property (strong, nonatomic) CADisplayLink *displayLink;

@property (strong, nonatomic) UILabel *firstCandidateNameLabel;
@property (strong, nonatomic) UILabel *firstCandidateVotesLabel;
@property (strong, nonatomic) CAShapeLayer *firstCandidateFirstPreferenceLayer;
@property (strong, nonatomic) CAShapeLayer *firstCandidateTotalLayer;

@property (strong, nonatomic) UILabel *secondCandidateNameLabel;
@property (strong, nonatomic) UILabel *secondCandidateVotesLabel;
@property (strong, nonatomic) CAShapeLayer *secondCandidateFirstPreferenceLayer;
@property (strong, nonatomic) CAShapeLayer *secondCandidateTotalLayer;


@property (strong, nonatomic) NSArray *pollingBooths;

@end
