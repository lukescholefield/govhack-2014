//
//  GHPollingPlaceViewController.h
//  GovHack
//
//  Created by Luke Scholefield on 12/07/2014.
//  Copyright (c) 2014 Luke Scholefield. All rights reserved.
//

#import "GHViewController.h"
#import <MapKit/MapKit.h>

@interface GHPollingPlaceViewController : GHViewController <MKMapViewDelegate>

@property (strong, nonatomic) NSDictionary *dictionary;
@property (assign, nonatomic) CLLocationCoordinate2D coordinate;
@property (strong, nonatomic) MKMapView *mapView;
@end
