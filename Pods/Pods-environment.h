
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Simple-KML
#define COCOAPODS_POD_AVAILABLE_Simple_KML
#define COCOAPODS_VERSION_MAJOR_Simple_KML 0
#define COCOAPODS_VERSION_MINOR_Simple_KML 1
#define COCOAPODS_VERSION_PATCH_Simple_KML 0

// TouchXML
#define COCOAPODS_POD_AVAILABLE_TouchXML
#define COCOAPODS_VERSION_MAJOR_TouchXML 0
#define COCOAPODS_VERSION_MINOR_TouchXML 1
#define COCOAPODS_VERSION_PATCH_TouchXML 0

// objective-zip
#define COCOAPODS_POD_AVAILABLE_objective_zip
#define COCOAPODS_VERSION_MAJOR_objective_zip 0
#define COCOAPODS_VERSION_MINOR_objective_zip 0
#define COCOAPODS_VERSION_PATCH_objective_zip 1

